import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  form = {
    email: '',
    password: '',
  };

  onSubmit() {
    console.log(this.form);
  }
}
